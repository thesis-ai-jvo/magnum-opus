<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Magnum-Opus, system for analyzing userdata
  ~ Copyright (C) 2018 Wouter Doeland
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>nl.wouterdoeland</groupId>
    <artifactId>magnum-opus</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <kotlin.version>1.2.61</kotlin.version>
        <ktor.version>0.9.4</ktor.version>
    </properties>

    <repositories>
        <repository>
            <id>ktor</id>
            <url>http://dl.bintray.com/kotlin/ktor</url>
        </repository>
        <repository>
            <id>kotlinx</id>
            <url>http://dl.bintray.com/kotlin/kotlinx</url>
        </repository>
        <repository>
            <id>jcenter</id>
            <url>http://jcenter.bintray.com</url>
        </repository>
        <repository>
            <id>bintray-kotlin-exposed</id>
            <name>bintray</name>
            <url>https://dl.bintray.com/kotlin/exposed</url>
        </repository>
    </repositories>

    <dependencies>
        <!-- Kotlin std lib -->
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
            <version>${kotlin.version}</version>
        </dependency>

        <!-- GSON -->
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.8.5</version>
        </dependency>

        <!-- Guava -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>25.1-jre</version>
        </dependency>

        <!-- Jetbrains/Exposed -->
        <dependency>
            <groupId>org.jetbrains.exposed</groupId>
            <artifactId>exposed</artifactId>
            <version>0.9.1</version>
        </dependency>

        <!-- Kotlin Coroutines -->
        <!--<dependency>-->
            <!--<groupId>org.jetbrains.kotlinx</groupId>-->
            <!--<artifactId>kotlinx-coroutines-core</artifactId>-->
            <!--<version>LATEST</version>-->
        <!--</dependency>-->

        <dependency>
            <groupId>io.ktor</groupId>
            <artifactId>ktor-server-netty</artifactId>
            <version>${ktor.version}</version>
        </dependency>

        <dependency>
            <groupId>io.ktor</groupId>
            <artifactId>ktor-gson</artifactId>
            <version>${ktor.version}</version>
        </dependency>

        <!-- MariaDB driver -->
        <dependency>
            <groupId>org.mariadb.jdbc</groupId>
            <artifactId>mariadb-java-client</artifactId>
            <version>2.1.2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.25</version>
        </dependency>

        <!-- Kotlin Logging (https://github.com/MicroUtils/kotlin-logging) -->
        <dependency>
            <groupId>io.github.microutils</groupId>
            <artifactId>kotlin-logging</artifactId>
            <version>1.4.6</version>
        </dependency>

        <!-- JUnit 5 -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>RELEASE</version>
            <scope>test</scope>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.h2database/h2 -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>1.4.197</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
        <testSourceDirectory>${project.basedir}/src/test/kotlin</testSourceDirectory>

        <plugins>
            <plugin>
                <artifactId>kotlin-maven-plugin</artifactId>
                <groupId>org.jetbrains.kotlin</groupId>
                <version>${kotlin.version}</version>

                <executions>
                    <execution>
                        <id>compile</id>
                        <goals> <goal>compile</goal> </goals>
                    </execution>

                    <execution>
                        <id>test-compile</id>
                        <goals> <goal>test-compile</goal> </goals>
                    </execution>
                </executions>

                <configuration>
                    <jvmTarget>1.8</jvmTarget>
                    <args>
                        <arg>-Xcoroutines=enable</arg>
                    </args>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>