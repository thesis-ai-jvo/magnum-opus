/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

Vue.component('modal', {
    template: '#modal-template'
});

Vue.component('delete', {
    template: '#delete-template'
});

var app = new Vue({
    el: '#app',
    data: {
        baseUrl: 'http://localhost:8080/',
        openSettings: false,
        openListFeatures: true,
        shownFeatures: {
            feature: true
        },
        configs: [],
        inserters: [],
        availableInserters: [],
        computers: [],
        availableComputers: [],
        renderers: [],
        availableRenderers: [],
        renderFeatures: [],
        newInserter: false,
        editedInserter: {},
        newComputer: false,
        editedComputer: {},
        newConfig: false,
        editedConfig: {},
        newRenderer: false,
        editedRenderer: {},
        newRenderFeature: false,
        editedRenderFeature: {},
        deleteType: '',
        deleteId: 0,
        deleteData: '',
        deleteObject: false,

        featureQuery: {
            inserters: [],
            entities: [],
            locations: [],
            offset: 0,
            limit: 50
        },

        fetchedFeatures: [],
        fetchedEntities: [],
        fetchedLocations: []
    },
    mounted() {
        this.getItems()
    },
    methods: {
        getItems() {
            this.getConfigs();
            this.getInserters();
            this.getAvailableInserters();
            this.getComputers();
            this.getAvailableComputers();
            this.getRenderers();
            this.getAvailableRenderers();
            this.getRenderFeatures();

            this.getAllEntities();
            this.getAllLocations();

            this.executeFeatureQuery();
        },
        getConfigs() {
            axios.get(this.baseUrl + 'manage/config/list')
                .then(json => {
                    this.configs = json.data.configs
                })
        },
        getInserters() {
            axios.get(this.baseUrl + 'manage/inserter/list')
                .then(json => {
                    this.inserters = json.data.inserters
                })
        },
        getAvailableInserters() {
            axios.get(this.baseUrl + 'manage/inserter/available')
                .then(json => {
                    this.availableInserters = json.data.availableInserters
                })
        },
        getComputers() {
            axios.get(this.baseUrl + 'manage/computer/list')
                .then(json => {
                    this.computers = json.data.computers
                })
        },
        getAvailableComputers() {
            axios.get(this.baseUrl + 'manage/computer/available')
                .then(json => {
                    this.availableComputers = json.data.availableComputers
                })
        },
        getRenderers() {
            axios.get(this.baseUrl + 'manage/renderer/list')
                .then(json => {
                    this.renderers = json.data.renderers
                })
        },
        getAvailableRenderers() {
            axios.get(this.baseUrl + 'manage/renderer/available')
                .then(json => {
                    this.availableRenderers = json.data.availableRenderers
                })
        },
        getRenderFeatures() {
            axios.get(this.baseUrl + 'manage/renderfeature/list')
                .then(json => {
                    this.renderFeatures = json.data.renderFeatures
                })
        },
        getCachedInserter(id) {
            return this.inserters.filter(inserter => inserter.id === id)[0]
        },
        addInserter() {
            this.editedInserter = {};
            this.newInserter = true;
        },
        submitNewInserter() {
            axios.post(this.baseUrl + 'manage/inserter/create', this.editedInserter)
                .then(json => {
                    this.inserters.push(json.data);
                    this.newInserter = false;
                })
                .catch(error => {
                    alert("Something went wrong while trying to create new object.")
                })
        },
        editInserter(id) {
            console.log("Edit inserter: " + id)
        },
        addComputer() {
            this.editedComputer = {};
            this.editedComputer.enabled = true;
            this.newComputer = true;
        },
        submitNewComputer() {
            axios.post(this.baseUrl + 'manage/computer/create', this.editedComputer)
                .then(json => {
                    this.inserters.push(json.data.inserter);
                    this.computers.push(json.data.computer);
                    this.newComputer = false;
                })
                .catch(error => {
                    alert("Something went wrong while trying to create new object.")
                })
        },
        editComputer(id) {
            console.log("Edit computer: " + id)
        },
        addConfig() {
            this.editedConfig = {
                "config": "{ \"parameters\": {}, \"flags\": [] }"
            };
            this.newConfig = true;
        },
        submitNewConfig() {
            axios.post(this.baseUrl + 'manage/config/create', JSON.parse(this.editedConfig.config))
                .then(json => {
                    this.configs.push(json.data);
                    this.newConfig = false;
                })
                .catch(error => {
                    alert("Something went wrong while trying to create new object.")
                })
        },
        editConfig(id) {
            console.log("Edit config: " + id)
        },
        addRenderer() {
            this.editedRenderer = {};
            this.editedRenderer.enabled = true;
            this.newRenderer = true;
        },
        submitNewRenderer() {
            axios.post(this.baseUrl + 'manage/renderer/create', this.editedRenderer)
                .then(json => {
                    this.inserters.push(json.data.inserter);
                    this.renderers.push(json.data.renderer);
                    this.newRenderer = false;
                })
                .catch(error => {
                    alert("Something went wrong while trying to create new object.")
                })
        },
        editRenderer(id) {
            console.log("Edit renderer: " + id)
        },
        startRender(id) {
            axios.put(this.baseUrl + 'manage/renderer/' + id)
                .then(json => {
                    alert("Started render!")
                })
        },
        addRenderFeature() {
            this.editedRenderFeature = {};
            this.editedRenderFeature.enabled = true;
            this.newRenderFeature = true;
        },
        submitNewRenderFeature() {
            axios.post(this.baseUrl + 'manage/renderfeature/create', this.editedRenderFeature)
                .then(json => {
                    this.renderFeatures.push(json.data);
                    this.newRenderFeature = false;
                })
                .catch(error => {
                    alert("Something went wrong while trying to create new object.")
                })
        },
        editRenderFeature(id) {
            console.log("Edit renderFeature: " + id)
        },
        deleteItem(id, type, data) {
            this.deleteType = type;
            this.deleteId = id;
            this.deleteData = data;
            this.deleteObject = true;
        },
        confirmDelete() {
            axios.delete(this.baseUrl + 'manage/' + this.deleteType + '/' + this.deleteId)
                .then(json => {
                    this.deleteObject = false;
                    this.getItems();
                })
                .catch(error => {
                    alert('Kon dit object niet verwijderen.');
                    console.log(error.message);
                })
        },


        getAllFeatures() {
            axios.get(this.baseUrl + 'list/feature')
                .then(json => {
                    this.fetchedFeatures = json.data.features
                })
        },
        getAllEntities() {
            axios.get(this.baseUrl + 'list/entity')
                .then(json => {
                    this.fetchedEntities = json.data.entities
                })
        },
        getAllLocations() {
            axios.get(this.baseUrl + 'list/location')
                .then(json => {
                    this.fetchedLocations = json.data.locations
                })
        },

        executeFeatureQuery() {
            axios.post(this.baseUrl + 'list/feature', this.featureQuery)
                .then(json => {
                    this.fetchedFeatures = json.data.features
                })
        }
    }
});