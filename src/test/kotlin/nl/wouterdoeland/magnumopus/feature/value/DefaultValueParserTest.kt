/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.value

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

internal class ValueParserTest {
    @Test
    fun testParse() {
        assertEquals(15, ValueParser.parse("int", "15"))
        assertEquals(42425, ValueParser.parse("int", "42425"))
        assertEquals(-5, ValueParser.parse("int", "-5"))
        assertThrows(ValueParseException::class.java, { ValueParser.parse("int", "kapot") })

        assertEquals(15.1, ValueParser.parse("double", "15.1") as Double, 0.1)
        assertEquals(42425.5, ValueParser.parse("double", "42425.5") as Double, 0.1)
        assertEquals(-5.65, ValueParser.parse("double", "-5.65") as Double, 0.1)
        assertThrows(ValueParseException::class.java, { ValueParser.parse("double", "kapot") })

        assertEquals("15.1", ValueParser.parse("string", "15.1"))
        assertEquals("test123", ValueParser.parse("string", "test123"))

        assertThrows(ValueTypeNotFoundException::class.java, { ValueParser.parse("deze vind je niet lol", "test") })
    }

    @Test
    fun testAddValueType() {
        ValueParser.addValueType(ValueType("long") { it.toLongOrNull() })

        assertEquals(17L, ValueParser.parse("long", "17"))
    }
}