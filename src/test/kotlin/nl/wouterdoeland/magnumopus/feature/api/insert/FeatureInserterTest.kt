/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api.insert

import kotlinx.coroutines.experimental.runBlocking
import nl.wouterdoeland.magnumopus.database.config.DatabaseConfigTest
import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.compute.FeatureComputerTest
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

internal class FeatureInserterTest {
    companion object {
        const val TEST_ENTITY_NAME_ONE = "First Entity"
        const val TEST_ENTITY_NAME_TWO = "Second Entity"
        const val TEST_LOCATION_ONE = "Location One"
        const val TEST_LOCATION_TWO = "Location Two"

        val inserterType by lazy {
            transaction {
                FeatureType.new {
                    this.valueType = FeatureComputerTest.TestFeatureInserter.getType().type
                    this.name = FeatureComputerTest.TestFeatureInserter.getType().type
                }
            }
        }
        val inserterConfig by lazy {
            transaction {
                Config.new {
                    this.config = FeatureConfig().toJson()
                }
            }
        }
        val featureInserter by lazy { transaction {
            Inserter.new {
                this.name = FeatureComputerTest.TestFeatureInserter.getName()
                this.type = inserterType
                this.config = inserterConfig
            }
        } }

        @BeforeAll
        @JvmStatic
        fun setup() {
            DatabaseConfigTest.setup()
            TestFeatureInserter.setup()
        }
    }


    object TestFeatureInserter: FeatureInserter() {
        override fun getType(): ValueType = ValueType.STRING

        override fun getName(): String = "TEST_INSERTER"

        override suspend fun run(inserter: Inserter) {
            insert(
                    getEntity(TEST_ENTITY_NAME_ONE),
                    getLocation(TEST_LOCATION_ONE),
                    "$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_ONE.test.",
                    inserter = inserter
            )
            insert(
                    getEntity(TEST_ENTITY_NAME_TWO),
                    getLocation(TEST_LOCATION_ONE),
                    "$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_ONE.test.",
                    inserter = inserter
            )
            insert(
                    getEntity(TEST_ENTITY_NAME_TWO),
                    getLocation(TEST_LOCATION_TWO),
                    "$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_TWO.test.",
                    inserter = inserter
            )
            insert(
                    getEntity(TEST_ENTITY_NAME_ONE),
                    getLocation(TEST_LOCATION_TWO),
                    "$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_TWO.test.",
                    inserter = inserter
            )
        }

        override fun getTypeName() = "string"
    }

    @Test
    fun getLocation() {
        assertEquals(TEST_LOCATION_ONE, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_ONE).location })
        assertEquals(TEST_LOCATION_ONE, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_ONE, TEST_LOCATION_ONE).location })
        assertEquals(TEST_LOCATION_ONE, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_ONE).location })
        assertEquals(TEST_LOCATION_TWO, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_TWO).location })
        assertEquals(TEST_LOCATION_TWO, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_TWO, TEST_LOCATION_TWO).location })
        assertEquals(TEST_LOCATION_TWO, transaction { TestFeatureInserter.getLocation(TEST_LOCATION_TWO).location })

        assertEquals(1, transaction { Location.find { Locations.name eq TEST_LOCATION_ONE }.count() })
        assertEquals(1, transaction { Location.find { Locations.name eq TEST_LOCATION_TWO }.count() })
    }

    @Test
    fun getEntity() {
        assertEquals(TEST_ENTITY_NAME_ONE, transaction { TestFeatureInserter.getEntity(TEST_ENTITY_NAME_ONE).name })
        assertEquals(TEST_ENTITY_NAME_ONE, transaction { TestFeatureInserter.getEntity(TEST_ENTITY_NAME_ONE).name })
        assertEquals(TEST_ENTITY_NAME_TWO, transaction { TestFeatureInserter.getEntity(TEST_ENTITY_NAME_TWO).name })
        assertEquals(TEST_ENTITY_NAME_TWO, transaction { TestFeatureInserter.getEntity(TEST_ENTITY_NAME_TWO).name })

        assertEquals(1, transaction { Entity.find { Entities.name eq TEST_ENTITY_NAME_ONE }.count() })
        assertEquals(1, transaction { Entity.find { Entities.name eq TEST_ENTITY_NAME_TWO }.count() })
    }

    @Test
    fun insert() {
        runBlocking {
            TestFeatureInserter.run(featureInserter)
        }

        assertEquals("$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_ONE.test.",
                transaction { Feature.find {
                    (Features.entity eq TestFeatureInserter.getEntity(TEST_ENTITY_NAME_ONE).id) and
                            (Features.location eq TestFeatureInserter.getLocation(TEST_LOCATION_ONE).id)
                }.first().parseValue() })

        assertEquals("$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_TWO.test.",
                transaction { Feature.find {
                    (Features.entity eq TestFeatureInserter.getEntity(TEST_ENTITY_NAME_ONE).id) and
                            (Features.location eq TestFeatureInserter.getLocation(TEST_LOCATION_TWO).id)
                }.first().parseValue() })

        assertEquals("$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_TWO.test.",
                transaction { Feature.find {
                    (Features.entity eq TestFeatureInserter.getEntity(TEST_ENTITY_NAME_TWO).id) and
                            (Features.location eq TestFeatureInserter.getLocation(TEST_LOCATION_TWO).id)
                }.first().parseValue() })

        assertEquals("$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_ONE.test.",
                transaction { Feature.find {
                    (Features.entity eq TestFeatureInserter.getEntity(TEST_ENTITY_NAME_TWO).id) and
                            (Features.location eq TestFeatureInserter.getLocation(TEST_LOCATION_ONE).id)
                }.first().parseValue() })

        runBlocking {
            TestFeatureInserter.run(featureInserter)
            TestFeatureInserter.run(featureInserter)
        }

        assertEquals(1, transaction { Feature.find { Features.value eq "$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_ONE.test." }.count() })
        assertEquals(1, transaction { Feature.find { Features.value eq "$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_ONE.test." }.count() })
        assertEquals(1, transaction { Feature.find { Features.value eq "$TEST_ENTITY_NAME_ONE/$TEST_LOCATION_TWO.test." }.count() })
        assertEquals(1, transaction { Feature.find { Features.value eq "$TEST_ENTITY_NAME_TWO/$TEST_LOCATION_TWO.test." }.count() })
    }
}