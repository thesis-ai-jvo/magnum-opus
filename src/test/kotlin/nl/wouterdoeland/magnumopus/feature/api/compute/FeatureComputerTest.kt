/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api.compute

import kotlinx.coroutines.experimental.runBlocking
import nl.wouterdoeland.magnumopus.database.config.DatabaseConfigTest
import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.FeatureRegistrar
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.api.insert.FeatureInserter
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

internal class FeatureComputerTest {
    companion object {
        val inserterType by lazy {
            transaction {
                FeatureType.new {
                    this.valueType = TestFeatureInserter.getType().type
                    this.name = TestFeatureInserter.getType().type
                }
            }
        }
        val computeType by lazy {
            transaction {
                FeatureType.new {
                    this.valueType = TestFeatureComputer.getType().type
                    this.name = TestFeatureComputer.getType().type
                }
            }
        }
        val inserterConfig by lazy {
            transaction {
                Config.new {
                    this.config = FeatureConfig().toJson()
                }
            }
        }
        val computeConfig by lazy {
            transaction {
                Config.new {
                    this.config = FeatureConfig(mapOf(TestFeatureComputer.KEY_MULTIPLIER to "3")).toJson()
                }
            }
        }
        val featureComputer by lazy { transaction {
            Inserter.new {
                this.name = TestFeatureComputer.getName()
                this.type = computeType
                this.config = computeConfig
            }
        } }
        val featureInserter by lazy { transaction {
            Inserter.new {
                this.name = TestFeatureInserter.getName()
                this.type = inserterType
                this.config = inserterConfig
            }
        } }

        @BeforeAll
        @JvmStatic
        fun setup() {
            DatabaseConfigTest.setup()
            TestFeatureInserter.setup()
            TestFeatureComputer.setup()
        }
    }

    object TestFeatureComputer: FeatureComputer() {
        const val KEY_MULTIPLIER = "multiplier"

        override fun getName() = "TEST_COMPUTER"

        override fun getType() = ValueType.INT

        override fun canHandleType(type: ValueType): Boolean = type == ValueType.INT

        override fun compute(feature: Feature, config: FeatureConfig) = transaction { feature.parseValue() as Int * (config.getParameter(KEY_MULTIPLIER)?.toIntOrNull() ?: 2) }

        override fun getTypeName() = "multiplied_int"
    }

    object TestFeatureInserter: FeatureInserter() {
        override fun getName() = "TEST_INSERTER_FOR_TEST_COMPUTER"

        override fun getType() = ValueType.INT

        override suspend fun run(inserter: Inserter) {
            insert(
                    getEntity("TEST_ENTITY"),
                    getLocation("TEST_LOCATION"),
                    8,
                    inserter = inserter
            )
            insert(
                    getEntity("TEST_ENTITY"),
                    getLocation("TEST_LOCATION"),
                    12,
                    inserter = inserter
            )
        }

        override fun getTypeName() = "int"
    }

    @Test
    fun compute() {
        val config = FeatureConfig(mapOf(TestFeatureComputer.KEY_MULTIPLIER to "3"))

        runBlocking {
            TestFeatureInserter.run(featureInserter)
        }

        transaction {
            val featureEight = Feature.find { Features.value eq "8" }.first()
            val featureTwelve = Feature.find { Features.value eq "12" }.first()

            assertEquals(24, TestFeatureComputer.compute(featureEight, config))
            assertEquals(36, TestFeatureComputer.compute(featureTwelve, config))
        }
    }

    @Test
    fun insertComputedFeature() {
        transaction {
            FeatureRegistrar.enableFeatureInserter(TestFeatureInserter, featureInserter)

            runBlocking {
                TestFeatureInserter.run(featureInserter)
            }

            FeatureRegistrar.registerFeatureComputer(TestFeatureComputer)

            transaction {
                FeatureComputerSetup.new {
                    this.inserter = featureInserter
                    this.computer = featureComputer
                    this.config = computeConfig
                    this.enabled = true
                }
            }

            val featureEight = Feature.find { Features.value eq "8" }.first()
            val featureTwelve = Feature.find { Features.value eq "12" }.first()

            FeatureRegistrar.computeAndInsert(featureEight, TestFeatureComputer, transaction { computeConfig.parseConfig() }, featureComputer)
            FeatureRegistrar.computeAndInsert(featureTwelve, TestFeatureComputer, transaction { computeConfig.parseConfig() }, featureComputer)

            assertEquals(1, transaction { Feature.find { (Features.value eq "24") and (Features.sourceFeature neq null) }.count() })
            assertEquals(1, transaction { Feature.find { (Features.value eq "36") and (Features.sourceFeature neq null) }.count() })

            runBlocking {
                TestFeatureInserter.run(featureInserter)
            }
        }
    }
}