/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.config

import nl.wouterdoeland.magnumopus.database.DatabaseUtil
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DatabaseConfigTest {
    companion object {
        val TEST_DATABASE_CONFIG = DatabaseConfig(
                "",
                "",
                "",
                "",
                "",
                "org.h2.Driver",
                "",
                "",
                191,
                {
                    "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE"
                }
        )

        val testDatabase by lazy { DatabaseUtil.connect(TEST_DATABASE_CONFIG) }

        fun connect() {
            testDatabase
        }

        fun setup() {
            connect()
            DatabaseUtil.createTables()
        }
    }

    @Test
    fun getJdbcUrl() {
        assertEquals("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", TEST_DATABASE_CONFIG.getJdbcUrl())
        assertEquals("jdbc:mysql://localhost:3306/test_database?test=true",
                DatabaseConfig(
                        "localhost",
                        "test_database",
                        "",
                        "",
                        "3306",
                        "com.mysql.jdbc.Driver",
                        jdbcSuffix = "?test=true").getJdbcUrl())
    }

    @Test
    fun testDatabase() {
        assert(testDatabase.connector().prepareStatement("SELECT H2VERSION();").executeQuery().next())
    }
}