/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database

import nl.wouterdoeland.magnumopus.database.config.DatabaseConfig
import nl.wouterdoeland.magnumopus.database.entity.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

object DatabaseUtil {
    var config: DatabaseConfig = DefaultDatabaseConfig

    private val tables = arrayOf(
            Entities,
            FeatureTypes,
            Inserters,
            Locations,
            FeatureTypes,
            Features,
            FeatureComputerSetups,
            RenderSetups,
            RenderFeatures,
            FeatureLinkers
    )

    fun connect(config: DatabaseConfig = this.config): Database {
        this.config = config

        return Database.connect(config.getJdbcUrl(), config.driver, config.username, config.password)
    }

    fun createTables() {
        transaction {
            SchemaUtils.create(*tables)
        }

        /*transaction {
            logger.addLogger(StdOutSqlLogger)

            tables.forEach {
                SchemaUtils.createMissingTablesAndColumns(it)
            }
        }*/
    }
}

fun Table.timestamp() = datetime("timestamp").clientDefault { DateTime.now() }

val MAX_VARCHAR_LENGTH by lazy { DatabaseUtil.config.maxVarcharLength }

val DefaultDatabaseConfig = DatabaseConfig("localhost", "magnumopus", "magnumopus", "magnumopus")
