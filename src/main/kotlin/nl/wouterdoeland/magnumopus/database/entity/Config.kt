/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import com.google.gson.Gson
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object Configs : IntIdTable() {
    val config = text("config")
}

class Config(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Config>(Configs)

    var config by Configs.config
    val inserters by Inserter referrersOn Inserters.config
    val featureComputers by FeatureComputerSetup referrersOn FeatureComputerSetups.config
    val renderers by RenderSetup referrersOn RenderSetups.config

    /**
     * Parse the config
     *
     * @return The parsed config
     */
    fun parseConfig(): FeatureConfig = Gson().fromJson(transaction { config }, FeatureConfig::class.java)

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}