/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import nl.wouterdoeland.magnumopus.database.MAX_VARCHAR_LENGTH
import nl.wouterdoeland.magnumopus.feature.value.ValueParser
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction

object FeatureTypes : IntIdTable() {
    val name = varchar("name", MAX_VARCHAR_LENGTH)
    val valueType = varchar("value_type", MAX_VARCHAR_LENGTH)
}

class FeatureType(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FeatureType>(FeatureTypes)

    var name by FeatureTypes.name
    var valueType by FeatureTypes.valueType

    /**
     * Parse a [value] for this type.
     *
     * @param value The value to parse
     *
     * @return The parsed value
     */
    fun parseValue(value: String) = ValueParser.parse(transaction { valueType }, value)

    /**
     * Create a string from a [value].
     *
     * @param value The value to stringify
     *
     * @return The stringified value
     */
    fun stringifyValue(value: Any) = ValueParser.stringify(transaction { valueType }, value)

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}