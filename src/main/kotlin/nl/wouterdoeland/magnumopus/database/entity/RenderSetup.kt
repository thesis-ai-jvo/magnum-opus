/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import nl.wouterdoeland.magnumopus.database.MAX_VARCHAR_LENGTH
import nl.wouterdoeland.magnumopus.feature.api.render.ConfiguredFeatureCollection
import nl.wouterdoeland.magnumopus.feature.api.render.LinkedFeatureCollection
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.transactions.transaction

object RenderSetups : IntIdTable() {
    val renderer = varchar("renderer", MAX_VARCHAR_LENGTH)
    val config = reference("config", Configs, ReferenceOption.CASCADE)
    val enabled = bool("enabled").default(true)
    val inserter = reference("inserter", Inserters, ReferenceOption.CASCADE)
}

class RenderSetup(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<RenderSetup>(RenderSetups)

    var renderer by RenderSetups.renderer
    var config by Config referencedOn RenderSetups.config
    var enabled by RenderSetups.enabled
    var inserter by Inserter referencedOn RenderSetups.inserter
    val renderFeatures by RenderFeature referrersOn RenderFeatures.renderer

    /**
     * Create a feature that links all the features together
     *
     * @return Returns a LinkedFeatureCollection with a link feature and the features
     */
    fun linkFeatures() = transaction {
            val features = hashMapOf<Inserter, ConfiguredFeatureCollection>()

            val collectionFeature = Feature.new {
                this.inserter = this@RenderSetup.inserter
                this.value = "LINK"
                this.location = null
                this.entity = null
                this.sourceFeature = null
            }

            renderFeatures.filter { it.enabled }.forEach { renderFeature ->
                val subCollectionFeature = Feature.new {
                    this.inserter = this@RenderSetup.inserter
                    this.value = "SUB_LINK"
                    this.location = null
                    this.entity = null
                    this.sourceFeature = collectionFeature
                }

                renderFeature.getFeatures().forEach { feature ->
                    FeatureLinker.new {
                        this.linkedFeature = subCollectionFeature
                        this.feature = feature
                    }
                }

                features[renderFeature.inserter] = ConfiguredFeatureCollection(renderFeature.getFeatures().toList(), renderFeature.config.parseConfig())
            }

            return@transaction LinkedFeatureCollection(collectionFeature, features)
        }

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}