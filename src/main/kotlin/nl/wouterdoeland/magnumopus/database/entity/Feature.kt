/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import nl.wouterdoeland.magnumopus.database.MAX_VARCHAR_LENGTH
import nl.wouterdoeland.magnumopus.database.timestamp
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

object Features : IntIdTable() {
    val entity = reference("entity", Entities, ReferenceOption.CASCADE).nullable()
    val location = reference("location", Locations, ReferenceOption.CASCADE).nullable()
    val inserter = reference("inserter", Inserters, ReferenceOption.CASCADE)
    val value = text("value")
    val internalId = varchar("internal_id", MAX_VARCHAR_LENGTH).uniqueIndex().nullable()
    val sourceFeature = reference("source", Features).nullable()
    val timestamp = timestamp()
}

class Feature(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Feature>(Features) {
        /**
         * Check whether a feature already exists
         *
         * @return True if the feature already exists
         */
        fun findFeature(entity: Entity?,
                        location: Location?,
                        inserter: Inserter,
                        value: String,
                        timestamp: DateTime?,
                        internalId: String?,
                        source: Feature?) = transaction {
                Feature.find {
                    (Features.entity eq entity?.id) and
                            (Features.location eq location?.id) and
                            (Features.inserter eq inserter.id) and
                            (Features.value eq value) and
                            (Features.internalId eq internalId) and
                            (Features.sourceFeature eq source?.id)
                }.firstOrNull { if(timestamp == null) { true } else { timestamp == it.timestamp } }
            }
    }

    var entity by Entity optionalReferencedOn Features.entity
    var location by Location optionalReferencedOn Features.location
    var inserter by Inserter referencedOn Features.inserter
    var value by Features.value
    var internalId by Features.internalId
    var sourceFeature by Feature optionalReferencedOn Features.sourceFeature
    var timestamp by Features.timestamp
    val linkedFeatures by FeatureLinker referrersOn FeatureLinkers.feature

    fun parseValue() = transaction { inserter.type.parseValue(value) }

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}