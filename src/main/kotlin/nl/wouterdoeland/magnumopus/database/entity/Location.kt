/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import nl.wouterdoeland.magnumopus.database.MAX_VARCHAR_LENGTH
import nl.wouterdoeland.magnumopus.database.timestamp
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Locations : IntIdTable() {
    val name = varchar("name", MAX_VARCHAR_LENGTH)
    val location = varchar("location", MAX_VARCHAR_LENGTH)
    val timestamp = timestamp()
}

class Location(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Location>(Locations)

    var name by Locations.name
    var location by Locations.location
    var timestamp by Locations.timestamp

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}