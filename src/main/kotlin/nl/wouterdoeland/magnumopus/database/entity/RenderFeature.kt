/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.database.entity

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.transactions.transaction

object RenderFeatures : IntIdTable() {
    val inserter = reference("inserter", Inserters, ReferenceOption.CASCADE)
    val config = reference("config", Configs, ReferenceOption.CASCADE)
    val renderer = reference("renderer", RenderSetups)
    val enabled = bool("enabled")
}

class RenderFeature(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<RenderFeature>(RenderFeatures)

    var inserter by Inserter referencedOn RenderFeatures.inserter
    var config by Config referencedOn RenderFeatures.config
    var renderer by RenderSetup referencedOn RenderFeatures.renderer
    var enabled by RenderFeatures.enabled

    /**
     * Get all the features that this renderfeature references
     *
     * @return All features that this renderfeature references
     */
    fun getFeatures() = transaction { inserter.features }

    override fun equals(other: Any?): Boolean {
        if (other !is IntEntity) return false

        return other.id.value == id.value
    }

    override fun hashCode() = id.value
}