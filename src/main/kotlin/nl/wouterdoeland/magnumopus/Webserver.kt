/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import mu.KotlinLogging
import nl.wouterdoeland.magnumopus.database.DatabaseUtil
import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.FeatureRegistrar
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.manage.api.list.entity.EntityListResponse
import nl.wouterdoeland.magnumopus.manage.api.list.entity.EntityResponse
import nl.wouterdoeland.magnumopus.manage.api.list.feature.FeatureListResponse
import nl.wouterdoeland.magnumopus.manage.api.list.feature.FeatureQueryRequest
import nl.wouterdoeland.magnumopus.manage.api.list.feature.FeatureResponse
import nl.wouterdoeland.magnumopus.manage.api.list.location.LocationListResponse
import nl.wouterdoeland.magnumopus.manage.api.list.location.LocationResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.OkObject
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.available.AvailableComputersResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.create.CreateComputerRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.create.CreateComputerResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.get.ComputerResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.list.ComputerListResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.computer.post.ComputerPostRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.config.get.ConfigResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.config.list.ConfigListResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.inserter.available.AvailableInsertersResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.inserter.create.CreateInserterRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.inserter.get.InserterResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.inserter.list.InserterListResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.inserter.post.InserterPostRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.available.AvailableRenderersResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.create.CreateRendererRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.create.CreateRendererResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.get.RendererResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.list.RendererListResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderer.post.RendererPostRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.renderfeature.create.CreateRenderFeatureRequest
import nl.wouterdoeland.magnumopus.manage.api.manage.renderfeature.get.RenderFeatureResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderfeature.list.RenderFeatureListResponse
import nl.wouterdoeland.magnumopus.manage.api.manage.renderfeature.post.RenderFeaturePostRequest
import org.jetbrains.exposed.sql.AndOp
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.OrOp
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.event.Level
import java.time.Duration

private val logger = KotlinLogging.logger {  }

fun main(args: Array<String>) {
    logger.info { "Connecting to database" }

    DatabaseUtil.connect()

    logger.info { "Creating tables" }

    DatabaseUtil.createTables()

    embeddedServer(Netty, 8080, "0.0.0.0", emptyList(), {}, Application::runServer).start(true)
}

fun setup() {
    logger.info { "Connecting to database" }

    DatabaseUtil.connect()

    logger.info { "Creating tables" }

    DatabaseUtil.createTables()

    logger.info { "Registering inserters, computers and renders" }

    FeatureRegistrar.registerAll()

    logger.info { "Starting inserters" }

    FeatureRegistrar.startInserters()
}

fun Application.runServer() {
    setup()
    install(ContentNegotiation) {
        gson {
            setPrettyPrinting()
        }
    }
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Delete)
        method(HttpMethod.Put)
        allowCredentials = true
        allowSameOrigin = true
        anyHost()
        maxAge = Duration.ofDays(1)
    }
    install(CallLogging) {
        level = Level.INFO
    }
    install(StatusPages) {
        status(HttpStatusCode.NotFound) {
            call.respond(HttpStatusCode.NotFound, "404 not found.")
        }
        status(HttpStatusCode.Conflict) {
            call.respond(HttpStatusCode.Conflict, "409 conflict.")
        }
    }

    routing {
        route("manage") {
            route("config") {
                get("list") {
                    call.respond(ConfigListResponse(transaction { Config.all().map { ConfigResponse(it.id.value, it.parseConfig()) } }))
                }
                get("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@get

                    val config = transaction { Config.findById(id)?.parseConfig() } ?: return@get

                    call.respond(ConfigResponse(id, config))
                }
                post("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@post

                    val config = call.receive<FeatureConfig>()

                    val configEntity = transaction { Config.findById(id) } ?: return@post

                    transaction { configEntity.config = config.toJson() }

                    call.respond(ConfigResponse(id, config))
                }
                post("create") {
                    val config = call.receive<FeatureConfig>()

                    val configEntity = transaction { Config.new {
                        this.config = config.toJson()
                    } }

                    call.respond(ConfigResponse(transaction { configEntity.id.value }, transaction { configEntity.parseConfig() }))
                }
                delete("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@delete

                    val config = transaction { Config.findById(id) } ?: return@delete

                    if(transaction { !config.inserters.empty() || !config.featureComputers.empty() || !config.renderers.empty() }) {
                        call.respond(HttpStatusCode.Conflict)
                        return@delete
                    }

                    transaction { config.delete() }

                    call.respond(OkObject)
                }
            }
            route("inserter") {
                get("list") {
                    call.respond(transaction {
                        InserterListResponse(Inserter.all().map { inserter -> InserterResponse(
                                inserter.id.value,
                                inserter.name,
                                inserter.config.id.value,
                                inserter.type.id.value
                        ) })
                    })
                }
                get("available") {
                    call.respond(AvailableInsertersResponse(FeatureRegistrar.getInserters().map { it.getName() }))
                }
                get("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@get

                    val inserter = transaction { Inserter.findById(id) } ?: return@get

                    call.respond(InserterResponse(id, transaction { inserter.name }, transaction { inserter.config.id.value }, transaction { inserter.type.id.value }))
                }
                post("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@post

                    val inserter = call.receive<InserterPostRequest>()

                    val inserterEntity = transaction { Inserter.findById(id) } ?: return@post

                    val configEntity = transaction { Config.findById(inserter.config) } ?: return@post

                    transaction {
                        inserterEntity.config = configEntity
                        inserterEntity.enabled = inserter.enabled
                    }

                    if(!inserter.enabled)
                        FeatureRegistrar.stopFeatureInserter(inserterEntity)

                    call.respond(InserterResponse(id, transaction { inserterEntity.name }, transaction { inserterEntity.config.id.value }, transaction { inserterEntity.type.id.value }))
                }
                post("create") {
                    val inserter = call.receive<CreateInserterRequest>()

                    val inserterObject = FeatureRegistrar.getInserter(inserter.name) ?: return@post

                    val config = transaction { Config.findById(inserter.config) } ?: return@post

                    val inserterEntity = transaction { Inserter.new {
                        this.name = inserterObject.getName()
                        this.config = config
                        this.type = inserterObject.getFeatureType()
                    } }

                    FeatureRegistrar.enableFeatureInserter(inserterObject, inserterEntity)

                    call.respond(InserterResponse(transaction { inserterEntity.id.value }, transaction { inserterEntity.name }, transaction { inserterEntity.config.id.value }, transaction { inserterEntity.type.id.value }))
                }
                delete("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@delete

                    val inserter = transaction { Inserter.findById(id) } ?: return@delete

                    FeatureRegistrar.stopFeatureInserter(inserter)

                    transaction { inserter.delete() }

                    call.respond(OkObject)
                }
            }
            route("computer") {
                get("list") {
                    call.respond(ComputerListResponse(transaction { FeatureComputerSetup.all().map { ComputerResponse(
                            it.id.value,
                            it.computer.id.value,
                            it.inserter.id.value,
                            it.config.id.value,
                            it.enabled
                    ) } }))
                }
                get("available") {
                    call.respond(AvailableComputersResponse(FeatureRegistrar.getComputers().map {
                        it.getName()
                    }))
                }
                get("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@get

                    val computer = transaction { FeatureComputerSetup.findById(id) } ?: return@get

                    call.respond(transaction { ComputerResponse(
                            computer.id.value,
                            computer.computer.id.value,
                            computer.inserter.id.value,
                            computer.config.id.value,
                            computer.enabled
                    ) })
                }
                post("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@post

                    val computer = call.receive<ComputerPostRequest>()

                    val computerEntity = transaction { FeatureComputerSetup.findById(id) } ?: return@post

                    val configEntity = transaction { Config.findById(computer.config) } ?: return@post

                    transaction {
                        computerEntity.config = configEntity
                        computerEntity.enabled = computer.enabled
                    }

                    call.respond(transaction { ComputerResponse(
                            computerEntity.id.value,
                            computerEntity.computer.id.value,
                            computerEntity.inserter.id.value,
                            computerEntity.config.id.value,
                            computerEntity.enabled
                    ) })
                }
                post("create") {
                    val computer = call.receive<CreateComputerRequest>()

                    val configEntity = transaction { Config.findById(computer.config) } ?: return@post
                    val inserterEntity = transaction { Inserter.findById(computer.inserter) } ?: return@post

                    val computerObject = FeatureRegistrar.getComputer(computer.computer) ?: return@post

                    val computerInserterEntity = transaction {
                        Inserter.new {
                            this.name = computerObject.getName()
                            this.type = computerObject.getFeatureType()
                            this.config = configEntity
                        }
                    }

                    val computerEntity = transaction {
                        FeatureComputerSetup.new {
                            this.computer = computerInserterEntity
                            this.inserter = inserterEntity
                            this.config = configEntity
                            this.enabled = computer.enabled
                        }
                    }

                    call.respond(CreateComputerResponse(transaction { ComputerResponse(
                            computerEntity.id.value,
                            computerEntity.computer.id.value,
                            computerEntity.inserter.id.value,
                            computerEntity.config.id.value,
                            computerEntity.enabled
                    ) }, transaction { InserterResponse(
                            computerInserterEntity.id.value,
                            computerInserterEntity.name,
                            computerInserterEntity.config.id.value,
                            computerInserterEntity.type.id.value
                    ) }))
                }
                delete("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@delete

                    val computer = transaction { FeatureComputerSetup.findById(id) } ?: return@delete

                    transaction {
                        computer.computer.delete()
                        computer.delete()
                    }

                    call.respond(OkObject)
                }
            }
            route("renderer") {
                get("list") {
                    call.respond(RendererListResponse(transaction { RenderSetup.all().map { RendererResponse(
                            it.id.value,
                            it.renderer,
                            it.config.id.value,
                            it.enabled,
                            it.inserter.id.value,
                            it.renderFeatures.map { it.id.value }
                    ) } }))
                }
                get("available") {
                    call.respond(AvailableRenderersResponse(FeatureRegistrar.getRenderers().map {
                        it.getName()
                    }))
                }
                get("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@get

                    val renderer = transaction { RenderSetup.findById(id) } ?: return@get

                    call.respond(transaction { RendererResponse(
                            renderer.id.value,
                            renderer.renderer,
                            renderer.config.id.value,
                            renderer.enabled,
                            renderer.inserter.id.value,
                            renderer.renderFeatures.map { it.id.value }
                    ) })
                }
                post("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@post

                    val renderer = call.receive<RendererPostRequest>()

                    val configEntity = transaction { Config.findById(renderer.config) } ?: return@post

                    val rendererEntity = transaction { RenderSetup.findById(id) } ?: return@post

                    transaction {
                        rendererEntity.config = configEntity
                        rendererEntity.enabled = renderer.enabled
                    }

                    call.respond(transaction { RendererResponse(
                            rendererEntity.id.value,
                            rendererEntity.renderer,
                            rendererEntity.config.id.value,
                            rendererEntity.enabled,
                            rendererEntity.inserter.id.value,
                            rendererEntity.renderFeatures.map { it.id.value }
                    ) })
                }
                post("create") {
                    val renderer = call.receive<CreateRendererRequest>()

                    val configEntity = transaction { Config.findById(renderer.config) } ?: return@post

                    val rendererObject = FeatureRegistrar.getRenderer(renderer.renderer) ?: return@post

                    val rendererInserterEntity = transaction { Inserter.new {
                        this.name = rendererObject.getName()
                        this.type = rendererObject.getFeatureType()
                        this.config = configEntity
                    } }

                    val rendererEntity = transaction { RenderSetup.new {
                        this.renderer = rendererObject.getName()
                        this.config = configEntity
                        this.enabled = renderer.enabled
                        this.inserter = rendererInserterEntity
                    } }

                    call.respond(CreateRendererResponse(transaction { RendererResponse(
                            rendererEntity.id.value,
                            rendererEntity.renderer,
                            rendererEntity.config.id.value,
                            rendererEntity.enabled,
                            rendererEntity.inserter.id.value,
                            rendererEntity.renderFeatures.map { it.id.value }
                    ) }, transaction {
                        InserterResponse(
                            rendererInserterEntity.id.value,
                            rendererInserterEntity.name,
                            rendererInserterEntity.config.id.value,
                            rendererInserterEntity.type.id.value
                    ) }))
                }
                delete("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@delete

                    val renderer = transaction { RenderSetup.findById(id) } ?: return@delete

                    transaction { renderer.delete() }

                    call.respond(OkObject)
                }
                put("{id}") {
                    // start a render
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@put

                    val renderer = transaction { RenderSetup.findById(id) } ?: return@put

                    FeatureRegistrar.startRender(renderer) {
                        // TODO: Sent this async to the client.
                        println("Render complete: " + transaction { it.value })

//                        call.respond(transaction {
//                            FeatureResponse(
//                                    it.id.value,
//                                    it.entity?.id?.value,
//                                    it.location?.id?.value,
//                                    it.inserter.id.value,
//                                    it.value,
//                                    it.internalId,
//                                    it.sourceFeature?.id?.value,
//                                    it.timestamp.toDate()
//                            )
//                        })
                    }

                    call.respond(OkObject)
                }
            }
            route("renderfeature") {
                get("list") {
                    call.respond(RenderFeatureListResponse(transaction { RenderFeature.all().map { RenderFeatureResponse(
                            it.id.value,
                            it.inserter.id.value,
                            it.config.id.value,
                            it.renderer.id.value,
                            it.enabled
                    ) } }))
                }
                get("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@get

                    val renderFeature = transaction { RenderFeature.findById(id) } ?: return@get

                    call.respond(transaction { RenderFeatureResponse(
                            renderFeature.id.value,
                            renderFeature.inserter.id.value,
                            renderFeature.config.id.value,
                            renderFeature.renderer.id.value,
                            renderFeature.enabled
                    ) })
                }
                post("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@post

                    val renderFeature = call.receive<RenderFeaturePostRequest>()

                    val configEntity = transaction { Config.findById(renderFeature.config) } ?: return@post

                    val renderFeatureEntity = transaction { RenderFeature.findById(id) } ?: return@post

                    transaction {
                        renderFeatureEntity.config = configEntity
                        renderFeatureEntity.enabled = renderFeature.enabled
                    }

                    call.respond(transaction { RenderFeatureResponse(
                            renderFeatureEntity.id.value,
                            renderFeatureEntity.inserter.id.value,
                            renderFeatureEntity.config.id.value,
                            renderFeatureEntity.renderer.id.value,
                            renderFeatureEntity.enabled
                    ) })
                }
                post("create") {
                    val renderFeature = call.receive<CreateRenderFeatureRequest>()

                    val configEntity = transaction { Config.findById(renderFeature.config) } ?: return@post
                    val rendererEntity = transaction { RenderSetup.findById(renderFeature.renderer) } ?: return@post
                    val inserterEntity = transaction { Inserter.findById(renderFeature.inserter) } ?: return@post

                    val renderFeatureEntity = transaction { RenderFeature.new {
                        this.renderer = rendererEntity
                        this.inserter = inserterEntity
                        this.config = configEntity
                        this.enabled = renderFeature.enabled
                    } }

                    call.respond(transaction { RenderFeatureResponse(
                            renderFeatureEntity.id.value,
                            renderFeatureEntity.inserter.id.value,
                            renderFeatureEntity.config.id.value,
                            renderFeatureEntity.renderer.id.value,
                            renderFeatureEntity.enabled
                    ) })
                }
                delete("{id}") {
                    val id = call.parameters["id"]?.toIntOrNull() ?: return@delete

                    val renderFeature = transaction { RenderFeature.findById(id) } ?: return@delete

                    transaction { renderFeature.delete() }

                    call.respond(OkObject)
                }
            }
        }

        route("list") {
            route("entity") {
                get {
                    call.respond(EntityListResponse(transaction { Entity.all().map { EntityResponse(
                            it.id.value,
                            it.name,
                            it.timestamp.toDate()
                    ) } }))
                }
            }
            route("feature") {
                get {
                    call.respond(FeatureListResponse(transaction { Feature.all().map { FeatureResponse(
                            it.id.value,
                            it.entity?.id?.value,
                            it.location?.id?.value,
                            it.inserter.id.value,
                            it.value,
                            it.internalId,
                            it.sourceFeature?.id?.value,
                            it.timestamp.toDate()
                    ) } }))
                }
                post {
                    val query = call.receive<FeatureQueryRequest>()

                    val ops = arrayListOf<List<Op<Boolean>>>()

                    ops.add(query
                            .entities
                            .map {
                                Op.build {
                                    Features.entity eq it
                                }
                            }
                    )

                    ops.add(query
                            .inserters
                            .map {
                                Op.build {
                                    Features.inserter eq it
                                }
                            }
                    )

                    ops.add(query
                            .locations
                            .map {
                                Op.build {
                                    Features.location eq it
                                }
                            }
                    )

                    val reducedOps = ops
                            .filter { opsList ->
                                opsList.isNotEmpty()
                            }
                            .map { opsList ->
                                opsList.reduce { acc, op ->
                                    OrOp(acc, op)
                                }
                            }

                    val op =
                            if(reducedOps.isNotEmpty())
                                reducedOps
                                        .reduce { acc, op ->
                                            AndOp(acc, op)
                                        }
                            else
                                null

                    val features = transaction { if(op == null) Feature.all() else Feature.find(op) }

                    call.respond(FeatureListResponse(transaction { features.limit(query.limit, query.offset).map { FeatureResponse(
                            it.id.value,
                            it.entity?.id?.value,
                            it.location?.id?.value,
                            it.inserter.id.value,
                            it.value,
                            it.internalId,
                            it.sourceFeature?.id?.value,
                            it.timestamp.toDate()
                    ) } }))
                }
            }
            route("location") {
                get {
                    call.respond(LocationListResponse(transaction { Location.all().map { LocationResponse(
                            it.id.value,
                            it.name,
                            it.location,
                            it.timestamp.toDate()
                    ) } }))
                }
            }
        }
    }
}