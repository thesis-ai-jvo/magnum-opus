/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api

import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.compute.FeatureComputer
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.api.insert.FeatureInserter
import nl.wouterdoeland.magnumopus.feature.api.render.Renderer
import nl.wouterdoeland.magnumopus.feature.features.computer.test.MinimumValueComputer
import nl.wouterdoeland.magnumopus.feature.features.inserter.test.RandomGradeFeature
import nl.wouterdoeland.magnumopus.feature.features.renderer.test.SortItemsRenderer
import nl.wouterdoeland.magnumopus.feature.value.ValueParser
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.concurrent.thread

object FeatureRegistrar {
    val AVAILABLE_INSERTERS = listOf<FeatureInserter>(
            RandomGradeFeature
    )
    val AVAILABLE_COMPUTERS = listOf<FeatureComputer>(
            MinimumValueComputer
    )
    val AVAILABLE_RENDERERS = listOf<Renderer>(
            SortItemsRenderer
    )

    private val featureInserters = hashMapOf<String, FeatureInserter>()
    private val featureComputers = hashMapOf<String, FeatureComputer>()
    private val renderers = hashMapOf<String, Renderer>()

    private val runners = hashMapOf<Inserter, Thread>()

    /**
     * Register all available features
     */
    fun registerAll() {
        AVAILABLE_INSERTERS.forEach {
            registerFeatureInserter(it)
        }
        AVAILABLE_COMPUTERS.forEach {
            registerFeatureComputer(it)
        }
        AVAILABLE_RENDERERS.forEach {
            registerRenderer(it)
        }
    }

    /**
     * Start all inserters with the corrent config
     */
    fun startInserters() {
        transaction {
            Inserter.all().toList()
        }.forEach {
            enableFeatureInserter(it)
        }
    }

    /**
     * Register a [renderer]. This will set this renderer up.
     *
     * @param renderer The renderer to insert
     */
    fun registerRenderer(renderer: Renderer) {
        ValueParser.addValueType(renderer.getType())

        renderer.setup()

        renderers[renderer.getName()] = renderer
    }

    /**
     * Register a [featureInserter].
     *
     * @param featureInserter The feature inserter to register
     */
    fun registerFeatureInserter(featureInserter: FeatureInserter) {
        featureInserters[featureInserter.getName()] = featureInserter
    }

    /**
     * Stop an [inserter].
     *
     * @param inserter The inserter to stop
     */
    fun stopFeatureInserter(inserter: Inserter) {
        runners[inserter]?.interrupt()
    }

    /**
     * Enable a [featureInserter]. This will set the feature up.
     *
     * @param featureInserter The feature inserter to register
     * @param inserter The inserter for the [featureInserter]
     */
    fun enableFeatureInserter(featureInserter: FeatureInserter, inserter: Inserter) {
        ValueParser.addValueType(featureInserter.getType())

        featureInserter.setup()

        featureInserters[featureInserter.getName()] = featureInserter

        if(transaction { inserter.enabled })
            runThread(featureInserter, inserter)
    }

    /**
     * Enable a new [inserter].
     *
     * @param inserter The inserter to enable
     */
    fun enableFeatureInserter(inserter: Inserter) {
        val featureInserter = featureInserters[transaction { inserter.name }]

        featureInserter?.let {
            enableFeatureInserter(featureInserter, inserter)
        }
    }

    /**
     * Register a [featureComputer]. This will set the feature up.
     *
     * @param featureComputer The feature computer to register
     */
    fun registerFeatureComputer(featureComputer: FeatureComputer) {
        featureComputer.setup()

        featureComputers[featureComputer.getName()] = featureComputer
    }

    private fun runThread(featureInserter: FeatureInserter, inserter: Inserter) {
        if(!runners.containsKey(inserter)) {
            runners[inserter] = thread(start = true, isDaemon = true) {
                runBlocking {
                    featureInserter.run(inserter)
                }
            }
        }
    }

    fun getInserters() = featureInserters.values.toTypedArray()

    fun getComputers() = featureComputers.values.toTypedArray()

    fun getRenderers() = renderers.values.toTypedArray()

    fun getComputer(name: String) = featureComputers[name]

    fun getRenderer(name: String) = renderers[name]

    fun getInserter(name: String) = featureInserters[name]

    /**
     * Notify the system of a new [feature] that was inserted by the [inserter].
     *
     * @param inserter The inserter that inserted the feature
     * @param feature The feature that was inserted
     * @param async Compute async
     */
    fun notifyOfNewFeature(inserter: Inserter, feature: Feature, async: Boolean = true) {
        transaction { FeatureComputerSetup.find {
            FeatureComputerSetups.inserter eq inserter.id and FeatureComputerSetups.enabled
        }.toList() }.forEach { computerBase ->
            getComputer(transaction { computerBase.computer.name })?.let { computer ->
                val config = transaction { computerBase.config.parseConfig() }
                val computerInserter = transaction { computerBase.computer }

                if(async)
                    launch { computeAndInsert(feature, computer, config, computerInserter) }
                else
                    computeAndInsert(feature, computer, config, computerInserter)
            }
        }
    }

    /**
     * Start a render with a [renderSetup] and a possible [callback].
     *
     * @param renderSetup The render setup to render for
     * @param callback This callback will be called when the render is done
     */
    fun startRender(renderSetup: RenderSetup, callback: ((Feature) -> Unit)?) {
        val featureCollection = renderSetup.linkFeatures()

        getRenderer(transaction {
            renderSetup.renderer
        })?.let { renderer ->
            launch {
                renderer.render(featureCollection.features, transaction { renderSetup.config.parseConfig() }).let { result ->
                    val resultFeature = renderer.insertRender(result, featureCollection, transaction { renderSetup.inserter })

                    callback?.invoke(resultFeature)
                }
            }
        }
    }

    /**
     * Compute and insert a [feature] with a [computer] and a [config].
     *
     * @param feature The feature to compute
     * @param computer The computer
     * @param config The config for the computer
     */
    fun computeAndInsert(feature: Feature, computer: FeatureComputer, config: FeatureConfig, computerInserter: Inserter) {
        computer.insertComputedFeature(computer.compute(feature, config), feature, computerInserter)
    }
}