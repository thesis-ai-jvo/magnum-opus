/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api

import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.value.ValueParser
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

abstract class BaseFeature: IFeature {
    abstract override fun getType(): ValueType

    abstract override fun getName(): String

    override fun insert(entity: Entity?,
                        location: Location?,
                        value: Any,
                        timestamp: DateTime?,
                        internalId: String?,
                        source: Feature?,
                        inserter: Inserter): Feature {
        val stringValue = getFeatureType().stringifyValue(value)

        val feature = Feature.findFeature(entity, location, inserter, stringValue, timestamp, internalId, source)
                ?: transaction {
                    Feature.new {
                        this.entity = entity
                        this.location = location
                        this.inserter = inserter
                        this.value = stringValue
                        this.internalId = internalId
                        this.sourceFeature = source
                        if(timestamp != null)
                            this.timestamp = timestamp
                    }
                }

        FeatureRegistrar.notifyOfNewFeature(inserter, feature)

        return feature
    }

    override fun getLocation(name: String, location: String) = transaction {
        Location.find {
            (Locations.location eq location)
        }.firstOrNull() ?: Location.new {
            this.location = location
            this.name = name
        }
    }

    override fun getEntity(name: String) = transaction {
        Entity.find {
            Entities.name eq name
        }.firstOrNull() ?:
        Entity.new {
            this.name = name
        }
    }

    override fun getConfig(inserter: Inserter): FeatureConfig = transaction { inserter.config.parseConfig() }

    protected fun registerType() {
        ValueParser.addValueType(getType())
    }

    override fun equals(other: Any?) = other is IFeature && getName() == other.getName()
}