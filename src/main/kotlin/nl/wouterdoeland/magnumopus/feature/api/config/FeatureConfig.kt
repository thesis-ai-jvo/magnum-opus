/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api.config

import com.google.gson.Gson

class FeatureConfig(val parameters: Map<String, String> = mapOf(), val flags: List<String> = listOf()) {
    companion object {
        /**
         * Create a config from a [json] string.
         *
         * @param json The JSON file
         *
         * @return The config
         */
        fun fromJson(json: String) = Gson().fromJson(json, FeatureConfig::class.java)
    }

    /**
     * Check whether this config has [flag] set.
     *
     * @param flag The flag to check
     *
     * @return True if the flag is set
     */
    fun hasFlag(flag: String) = flags.contains(flag)

    /**
     * Get the value for a [parameter].
     *
     * @param parameter The parameter to get the value for
     *
     * @return The value for the [parameter] or null
     */
    fun getParameter(parameter: String) = parameters[parameter]

    /**
     * Convert this config to a JSON string.
     *
     * @return The JSON string.
     */
    fun toJson() = Gson().toJson(this)

    override fun hashCode() = toJson().hashCode()

    override fun equals(other: Any?) = other is FeatureConfig && other.toJson() == toJson()
}