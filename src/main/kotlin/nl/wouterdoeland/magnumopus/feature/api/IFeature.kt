/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api

import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.joda.time.DateTime

interface IFeature {
    /**
     * Insert a feature into the system.
     *
     * @param entity The entity to insert the feature for
     * @param location The location at which the feature happened
     * @param value The value of the feature
     * @param timestamp The timestamp at which the feature commenced
     * @param internalId The internal identifier for this feature
     * @param source The source feature for this feature
     *
     * @return The feature that was created or fetched (if it is duplicate)
     */
    fun insert(entity: Entity?,
               location: Location?,
               value: Any,
               timestamp: DateTime? = null,
               internalId: String? = null,
               source: Feature? = null,
               inserter: Inserter
    ): Feature

    /**
     * Get the name of this feature.
     *
     * @return The name of this feature
     */
    fun getName(): String

    /**
     * Get the type of this feature's value.
     *
     * @return The type of this feature's value
     */
    fun getType(): ValueType

    /**
     * Set up the feature. This function is run on enabling the feature
     */
    fun setup() {

    }

    /**
     * Get an entity by [name].
     *
     * @param name The name of the entity
     *
     * @return The entity for this [name]
     */
    fun getEntity(name: String): Entity

    /**
     * Get a location for this [name] and [location].
     *
     * @param name The name of this location
     * @param location The location of this location
     *
     * @return The location for this [name] and [location]
     */
    fun getLocation(name: String, location: String = name): Location

    /**
     * Get the config for this feature
     *
     * @return The config for this feature
     */
    fun getConfig(inserter: Inserter): FeatureConfig

    /**
     * Get the feature type name
     *
     * @return The feature type name
     */
    fun getTypeName(): String

    /**
     * Get the feature type for this feature
     *
     * @return The feature type for this feature
     */
    fun getFeatureType(): FeatureType = getType().getDatabaseValueType(getTypeName())
}