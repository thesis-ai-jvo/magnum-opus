/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api.render

import nl.wouterdoeland.magnumopus.database.entity.Feature
import nl.wouterdoeland.magnumopus.database.entity.Inserter
import nl.wouterdoeland.magnumopus.feature.api.BaseFeature
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.value.ValueType

abstract class Renderer: BaseFeature() {
    abstract fun render(features: Map<Inserter, ConfiguredFeatureCollection>, config: FeatureConfig): Any

    abstract fun canHandleType(type: ValueType): Boolean

    /**
     * Insert a render
     *
     * @param value The value that was rendered
     * @param featureCollection The feature collection this render was based on
     *
     * @return The feature that was created
     */
    fun insertRender(value: Any, featureCollection: LinkedFeatureCollection, inserter: Inserter): Feature {
        return insert(
                null,
                null,
                value,
                null,
                null,
                featureCollection.collectionFeature,
                inserter
        )
    }
}