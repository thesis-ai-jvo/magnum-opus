/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.api.compute

import nl.wouterdoeland.magnumopus.database.entity.Feature
import nl.wouterdoeland.magnumopus.database.entity.Inserter
import nl.wouterdoeland.magnumopus.feature.api.BaseFeature
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.transactions.transaction

abstract class FeatureComputer: BaseFeature() {
    /**
     * Calculate a result for a new [feature].
     *
     * @param feature The feature to compute a result for
     *
     * @return The computed result
     */
    abstract fun compute(feature: Feature, config: FeatureConfig): Any

    /**
     * Check whether this feature van handle this [type].
     *
     * @param type The type to check for
     *
     * @return Returns true if the [type] can be handled, false if not.
     */
    abstract fun canHandleType(type: ValueType): Boolean

    /**
     * Insert a [value] computed from a [source] feature.
     *
     * @param value The value that was computed
     * @param source The source that the value was computed from
     *
     * @return The feature
     */
    fun insertComputedFeature(value: Any, source: Feature, inserter: Inserter) =
            insert(
                    transaction { source.entity },
                    transaction { source.location },
                    value,
                    transaction {  source.timestamp },
                    transaction { source.internalId },
                    source,
                    inserter
            )

    override fun setup() {
        registerType()
    }
}