/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.value


object ValueParser {
    private val valueTypes: HashSet<ValueType> = hashSetOf(ValueType.INT, ValueType.DOUBLE, ValueType.STRING, ValueType.BOOLEAN)

    /**
     * Get all the value types.
     *
     * @return An array containing all the value types
     */
    fun getValueTypes() = valueTypes.toTypedArray()

    /**
     * Add a [valueType] to the value type collection.
     *
     * @param valueType The value type to add
     */
    fun addValueType(valueType: ValueType) {
        valueTypes.add(valueType)
    }

    fun getValueType(type: String) = getValueTypes()
            .firstOrNull { it.type.toLowerCase() == type.toLowerCase() }
            ?: throw ValueTypeNotFoundException(type)

    /**
     * Parses [value] to [type].
     *
     * @param value The value to parse
     * @param type The type to parse for
     *
     * @return The parsed value
     *
     * @throws ValueParseException Thrown if parsing failed.
     * @throws ValueTypeNotFoundException Thrown if the parser was not found.
     */
    fun parse(type: String, value: String) = getValueType(type)
            .parser
            .invoke(value)
            ?: throw ValueParseException(value, type)

    /**
     * Stringify [value] for [type].
     *
     * @param value The value to stringify
     * @param type The type to parse from
     *
     * @return The stringified value
     *
     * @throws ValueStringifierException Thrown if stringifieing failed
     * @throws ValueTypeNotFoundException Thrown if the stringifier was not found
     */
    fun stringify(type: String, value: Any) = getValueType(type)
            .stringifier
            .invoke(value)
            ?: throw ValueStringifierException(value, type)
}