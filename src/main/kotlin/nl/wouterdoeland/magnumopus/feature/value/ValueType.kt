/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.value

import com.google.gson.Gson
import nl.wouterdoeland.magnumopus.database.entity.FeatureType
import nl.wouterdoeland.magnumopus.database.entity.FeatureTypes
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

class ValueType(val type: String, val stringifier: (Any) -> String? = { it.toString() }, val parser: (String) -> Any?) {
    companion object {
        val INT = ValueType("int") {
            it.toIntOrNull()
        }
        val DOUBLE = ValueType("double") {
            it.toDoubleOrNull()
        }
        val STRING = ValueType("string") {
            it
        }
        val BOOLEAN = ValueType("boolean") {
            if(it.toBoolean()) {
                true
            } else {
                if(it.toLowerCase() == "false")
                    false
                else
                    null
            }
        }
        val DOUBLE_LIST = ValueType("double_list", {
            Gson().toJson(it)
        }) {
            Gson().fromJson<List<Double>>(it, List::class.java)
        }
        val STRING_LIST = ValueType("string_list", {
            Gson().toJson(it)
        }) {
            Gson().fromJson<List<String>>(it, List::class.java)
        }
    }

    fun getDatabaseValueType(name: String): FeatureType = transaction {
        FeatureType.find {
            FeatureTypes.name eq name and(FeatureTypes.valueType eq type)
        }.firstOrNull() ?: FeatureType.new {
            this.name = name
            this.valueType = type
        }
    }

    override fun hashCode() = type.hashCode()

    override fun equals(other: Any?) = other is ValueType && other.type == type
}