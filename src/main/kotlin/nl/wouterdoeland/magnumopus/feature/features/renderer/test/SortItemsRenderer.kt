/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.features.renderer.test

import nl.wouterdoeland.magnumopus.database.entity.Inserter
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.api.render.ConfiguredFeatureCollection
import nl.wouterdoeland.magnumopus.feature.api.render.Renderer
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.transactions.transaction

object SortItemsRenderer: Renderer() {
    val SORT_DESCENDING_KEY = "sort_descending"

    override fun render(features: Map<Inserter, ConfiguredFeatureCollection>, config: FeatureConfig): Any {
        val descendedSort = config.getParameter(SORT_DESCENDING_KEY)?.toLowerCase() == "true"

        val featureList = transaction { features.flatMap { it.value.features }.map { it.value } }

        return if(descendedSort) {
            featureList.sortedDescending()
        } else {
            featureList.sorted()
        }
    }

    override fun canHandleType(type: ValueType): Boolean = true

    override fun getType(): ValueType = ValueType.DOUBLE_LIST

    override fun getName(): String = "item_sorter"

    override fun getTypeName() = "sorted_list"
}