/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.features.computer.test

import nl.wouterdoeland.magnumopus.database.entity.Feature
import nl.wouterdoeland.magnumopus.feature.api.compute.FeatureComputer
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.api.config.InvalidConfigException
import nl.wouterdoeland.magnumopus.feature.api.type.InvalidTypeException
import nl.wouterdoeland.magnumopus.feature.value.ValueType

object MinimumValueComputer: FeatureComputer() {
    const val MINIMUM_KEY = "minimum"

    override fun compute(feature: Feature, config: FeatureConfig): Any {
        val minimum = config.getParameter(MINIMUM_KEY)?.toDoubleOrNull() ?: throw InvalidConfigException(MINIMUM_KEY)

        val parsedValue = feature.parseValue()

        val number = (parsedValue as? Number ?: throw InvalidTypeException(listOf("double", "int"))).toDouble()

        return number > minimum
    }

    override fun canHandleType(type: ValueType): Boolean = when(type) {
        ValueType.INT, ValueType.DOUBLE -> true
        else -> false
    }

    override fun getType() = ValueType.BOOLEAN

    override fun getName() = "minimum_value_computer"

    override fun getTypeName() = "sufficient"
}