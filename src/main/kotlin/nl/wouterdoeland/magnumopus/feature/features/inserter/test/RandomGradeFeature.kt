/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus.feature.features.inserter.test

import nl.wouterdoeland.magnumopus.database.entity.Entity
import nl.wouterdoeland.magnumopus.database.entity.Inserter
import nl.wouterdoeland.magnumopus.database.entity.Location
import nl.wouterdoeland.magnumopus.feature.api.insert.FeatureInserter
import nl.wouterdoeland.magnumopus.feature.value.ValueType
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*
import kotlin.math.absoluteValue

object RandomGradeFeature: FeatureInserter() {
    override fun getType(): ValueType = ValueType.DOUBLE

    override fun getName(): String = "random_grade_feature"

    private fun getRandomLocation(): Location {
        val randomLocation = "Classroom ${Random().nextInt(10)}"

        return getLocation(randomLocation)
    }

    private fun getRandomGrade(): Double {
        return ((Random().nextDouble().absoluteValue*15.0+3.0).let { if(it>10.0) 10.0 else it } - Random().nextDouble().absoluteValue*2.0)
    }

    private fun getRandomEntity(): Entity = getEntity(listOf("Wouter", "Tijs", "Jan").shuffled().first())

    override suspend fun run(inserter: Inserter) {
        while(true) {

            transaction {
                insert(
                        getRandomEntity(),
                        getRandomLocation(),
                        getRandomGrade(),
                        inserter = inserter
                )
            }

            Thread.sleep((Random().nextDouble()*10000).toLong())
        }
    }

    override fun getTypeName() = "grade"
}