/*
 * Magnum-Opus, system for analyzing userdata
 * Copyright (C) 2018 Wouter Doeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package nl.wouterdoeland.magnumopus

import com.google.common.base.Stopwatch
import mu.KotlinLogging
import nl.wouterdoeland.magnumopus.database.DatabaseUtil
import nl.wouterdoeland.magnumopus.database.entity.*
import nl.wouterdoeland.magnumopus.feature.api.FeatureRegistrar
import nl.wouterdoeland.magnumopus.feature.api.config.FeatureConfig
import nl.wouterdoeland.magnumopus.feature.api.render.Renderer
import nl.wouterdoeland.magnumopus.feature.features.computer.test.MinimumValueComputer
import nl.wouterdoeland.magnumopus.feature.features.inserter.test.RandomGradeFeature
import nl.wouterdoeland.magnumopus.feature.features.renderer.test.SortItemsRenderer
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.concurrent.TimeUnit

private val logger = KotlinLogging.logger {  }

fun main(args: Array<String>) {
    val stopwatch = Stopwatch.createStarted()

    logger.info { "Connecting to database" }

    DatabaseUtil.connect()

    logger.info { "Creating tables" }

    DatabaseUtil.createTables()

    logger.info { "Creating feature inserter feature in database" }

    val emptyConfig = transaction { Config.find { Configs.config eq FeatureConfig().toJson() }.firstOrNull()
            ?: Config.new {
                this.config = FeatureConfig().toJson()
            } }

    val minimumValueFeatureConfig = FeatureConfig(mapOf(MinimumValueComputer.MINIMUM_KEY to "3")).toJson()
    val minimumValueConfig = transaction { Config.find { Configs.config eq minimumValueFeatureConfig }.firstOrNull()
            ?: Config.new {
                this.config = minimumValueFeatureConfig
            } }

    val randomGradeFeatureInserter = transaction { Inserter.find { Inserters.name eq RandomGradeFeature.getName() }.firstOrNull()
            ?: Inserter.new {
                this.config = emptyConfig
                this.name = RandomGradeFeature.getName()
                this.type = RandomGradeFeature.getFeatureType()
            } }

    val minimumValueInserter = transaction { Inserter.find { Inserters.name eq MinimumValueComputer.getName() }.firstOrNull()
            ?: Inserter.new {
                this.config = minimumValueConfig
                this.name = MinimumValueComputer.getName()
                this.type = MinimumValueComputer.getFeatureType()
            } }

    val sortItemsInserter = transaction { Inserter.find { Inserters.name eq SortItemsRenderer.getName() }.firstOrNull()
            ?: Inserter.new {
                this.config = emptyConfig
                this.name = SortItemsRenderer.getName()
                this.type = SortItemsRenderer.getFeatureType()
            } }

    val sortItemsRenderer = transaction { RenderSetup.find { RenderSetups.renderer eq SortItemsRenderer.getName() }.firstOrNull()
            ?: RenderSetup.new {
                this.config = emptyConfig
                this.enabled = true
                this.inserter = sortItemsInserter
                this.renderer = SortItemsRenderer.getName()
            } }

    val sortItemsRenderFeature = transaction { RenderFeature.find { RenderFeatures.renderer eq sortItemsRenderer.id }.firstOrNull()
            ?: RenderFeature.new {
                this.config = emptyConfig
                this.enabled = true
                this.inserter = randomGradeFeatureInserter
                this.renderer = sortItemsRenderer
            } }

    logger.info { "Enabling features:" }

    logger.info { "\tInserters:" }

    FeatureRegistrar.enableFeatureInserter(RandomGradeFeature, randomGradeFeatureInserter)
    logger.info { "\t\tEnabled ${RandomGradeFeature.getName()}" }

    logger.info { "\tComputers:" }
    FeatureRegistrar.registerFeatureComputer(MinimumValueComputer)
    logger.info { "\t\tSet up ${MinimumValueComputer.getName()}" }

    logger.info { "\tCompute links:" }

    transaction {
        FeatureComputerSetup.find { FeatureComputerSetups.computer eq minimumValueInserter.id }.firstOrNull()
                ?: FeatureComputerSetup.new {
                    this.computer = minimumValueInserter
                    this.inserter = randomGradeFeatureInserter
                    this.config = minimumValueConfig
                    this.enabled = true
                }
    }
    logger.info { "\t\tInstalled link between ${RandomGradeFeature.getName()} and ${MinimumValueComputer.getName()}" }

    logger.info { "\tRenderers:" }

    DEFAULT_RENDERERS.forEach { renderer ->
        FeatureRegistrar.registerRenderer(renderer)
        logger.info { "\t\tSet up ${renderer.getName()}" }
    }

    logger.info { "Done, took ${stopwatch.elapsed(TimeUnit.MILLISECONDS)}ms." }

    var readLine = readLine()

    while (readLine != "stop") {
        when(readLine) {
            "render_list" -> {
                FeatureRegistrar.startRender(sortItemsRenderer) {
                    println("Sorted items! Result is: ${transaction { it.value }}")
                }
            }
        }

        readLine = readLine()
    }
}

val DEFAULT_RENDERERS = listOf<Renderer>(SortItemsRenderer)